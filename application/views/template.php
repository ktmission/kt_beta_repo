<!-- <?php session_start(); ?> -->
<!doctype html>
<html lang="en">
<body>
<?php include_once 'master/header.php'; ?>
<!--Search-->
<div id="search">
  <button type="button" class="close">×</button>
  <form>
    <input type="search" value="" placeholder="Search here...."  required/>
    <button type="submit" class="btn btn_common blue">Search</button>
  </form>
</div>
<!-- This will contain page content -->
  <?php echo $this->template->content; ?>
<!--FOOTER-->
<?php include_once 'master/footer.php'; ?>
</body>
</html>
