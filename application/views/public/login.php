<!doctype html>
<html lang="en">
<body style="background-color:#999da0;">
<!-- <?php  //include_once 'header.php'; ?> -->

<!--Search-->
<div id="search">
  <button type="button" class="close">×</button>
  <form>
    <input type="search" value="" placeholder="Search here...."  required/>
    <button type="submit" class="btn btn_common blue">Search</button>
  </form>
</div>

<section id="contact" class="padding">
  <div class="container">

    <div class="row padding-bottom" style="font-family: 'Source Sans Pro';">
    <center>  <div class="col-md-6 contact_address heading_space wow fadeInLeft" data-wow-delay="300ms">
<!--Log In -->
        <center><h2 class="heading heading_space">Enter In! <span class="divider-left"></span></h2></center>
        <form class="form-inline findus" id="login-form" method="post" style="width:70%;color:white;" action="<?php echo base_url();?>login_handler/verifyUserLoginCredentials">
          <div class="form-group"><h4><?php echo $this->session->flashdata('message');?></h4></div>
          <div class="form-group">
                <input type="email" class="form-control" placeholder="Email" name="loginEmail" id="loginEmail" required>
                <input type="password" class="form-control" placeholder="Password" name="loginpwd" id="loginpwd" required>
                <a href="#" style="float:right; color:black;">Forgot your password?</a>
                <p><input type="submit" class="btn_common red border_radius" style="color:white;" name="btn_login" value="Log In"></p>
          </div>
        </form>
      </div></center>
<!--Register -->
      <center><div class="col-md-6 wow fadeInRight" data-wow-delay="300ms">
        <h2 class="heading heading_space">Register Yourself Now!<span class="divider-left"></span></h2>
        <form class="form-inline findus" id="signup-form" method="post" style="width:70%;color:white;" action="<?php echo base_url();?>login_handler/setUserRegistration">
          <div class="form-group">
            <h4><?php echo $this->session->flashdata('SignupMsg');?></h4>
          </div>
            <div class="form-group">
                <input type="text" class="form-control" placeholder="First Name"  name="firstName" id="firstName" required>
                <input type="email" class="form-control" placeholder="Email" name="email" id="email" required>
                <input type="password" class="form-control" placeholder="Password" name="pwd" id="pwd" required>
                <input type="password" class="form-control" placeholder="Re-enter Password" name="repwd" id="repwd" required>
                <p align="left"><b>Register as:<t></b>
                                              <input type="radio" name="registerAs" value="1" checked> Student
                                              <input type="radio" name="registerAs" value="2"> Expert
                                              <input type="radio" name="registerAs" value="3"> University
                                              </p>
                <!-- binding country list -->
                <select class="form-control" id="countryid" name="countryid" required>
                  <option value="">Select Country</option>
                  <?php foreach ($countries as $countryList): ?>
                    <option value="<?php echo $countryList->id;?>"><?php echo $countryList->name;?></option>
                  <?php endforeach; ?>
                </select>
                <input type="text" class="form-control" placeholder="City" name="city" id="city" style="display:none;">
                <input type="text" class="form-control" placeholder="Phone number" name="pNumber" id="pNumber">
                <center><input type="submit" class="btn_common red border_radius" id="signup" name="signup" value="Register"></center>
            </div>
        </form>
  </div></center>
</div>
</section>
</body>
</html>
