<?php
 if(!isset($_SESSION))
 {
    session_start();
   }
 ?>
<!--Search-->
<div id="search">
  <button type="button" class="close">×</button>
  <form>
    <input type="search" value="" placeholder="Search here...."  required/>
    <button type="submit" class="btn btn_common blue">Search</button>
  </form>
</div>

<!--Page Header-->
<section class="page_header padding-top">
  <div class="container">
    <div class="row">
      <div class="col-md-12 page-content">
        <?php
         if(isset($_SESSION["user"]))
         { ?>
           <h1><?php echo $_SESSION["name"] ?>'S Profile</h1>
          <?php } ?>
        <p>KnowledgeTime offers live online courses by best experts</p>
        <div class="page_nav">
      <span>You are here:<a href="<?php echo base_url();?>home/index">Home</a> <span><i class="fa fa-angle-double-right"></i>Profile</span>
      </div>
      </div>
    </div>
  </div>
</section>
<div class="container">
  <div class="row">
    <form class="form-horizontal" method="post" Id="updateStudentProfile" action="<?php echo base_url();?>/users/student_dashboard/updateStudentProfile_CI">
    <div class="form-group">
    <div class="col-lg-2">
    </div>
  </div>
    <div class="form-group">
    <div class="col-lg-2">
      UserID:
    </div>
    <div class="col-lg-4">
    <input type="text" id="txtUserID" name="txtUserID" class="form-control" placeholder="User ID.." readonly="true" value="<?php echo $profile_data['login_userid']?>"/>
    </div>
  </div>
    <div class="form-group">
      <div class="col-lg-2">
        First Name:
      </div>
      <div class="col-lg-4">
      <input type="text" id="txtFirstName" name="txtFirstName" class="form-control" placeholder="First Name" value="<?php echo $profile_data['first_name'] ?>"/>
      </div>
    </div>
    <div class="form-group">
      <div class="col-lg-2">
        Middle Name:
      </div>
      <div class="col-lg-4">
      <input type="text" id="txtMiddleName" name="txtMiddleName" class="form-control" placeholder="Middle Name" value="<?php echo $profile_data['middle_name'] ?>"/>
      </div>
    </div>
    <div class="form-group">
      <div class="col-lg-2">
        Last Name:
      </div>
      <div class="col-lg-4">
      <input type="text" id="txtLastName" name="txtLastName" class="form-control" placeholder="Last Name" value="<?php echo $profile_data['last_name'] ?>"/>
      </div>
    </div>
    <div class="form-group">
      <div class="col-lg-2">
        Address Line1:
      </div>
      <div class="col-lg-4">
      <input type=text id="txtAddressLine1" name="txtAddressLine1" class="form-control" placeholder="Address Line 1"  value="<?php echo $profile_data['address1'] ?>"/>
      </div>
    </div>
    <div class="form-group">
      <div class="col-lg-2">
        Address Line2:
      </div>
      <div class="col-lg-4">
      <input type="text" id="txtAddressLine2" name="txtAddressLine2" class="form-control" placeholder="Address Line 2" value="<?php echo $profile_data['address2'] ?>"/>
      </div>
    </div>
    <div class="form-group">
      <div class="col-lg-2">
        Country:
      </div>
      <div class="col-lg-4">
        <select class="form-control" id="countryid" name="countryid" required>
          <option value="">Select Country</option>
          <?php foreach ($countries as $countryList): ?>
            <?php
              if($profile_data['country_id']==$countryList->id)
              {?>
                <option value="<?php echo $countryList->id;?>" selected><?php echo $countryList->name;?></option>
          <?php  }
            else   {?>
                <option value="<?php echo $countryList->id;?>"><?php echo $countryList->name;?>
            <?php } ?>
          <?php endforeach; ?>
        </select>
      </div>
    </div>
    <div class="form-group">
      <div class="col-lg-2">
        State:
      </div>
      <div class="col-lg-4">
        <select class="form-control" id="stateid" name="stateid">
          <option value="">Select State</option>
          <?php foreach ($state_list as $state): ?>
            <?php
              if($state_list['state_id']==$state->id)
              {?>
                <option value="<?php echo $state->id;?>" selected><?php echo $state->name;?></option>
          <?php  }
            else   {?>
                <option value="<?php echo $state->id;?>"><?php echo $state->name;?></option>
            <?php } ?>
          <?php endforeach; ?>
        </select>
      </div>
    </div>
    <div class="form-group" style="display:none;">
      <div class="col-lg-2">
        City:
      </div>
        </div>
    <div class="form-group">
      <div class="col-lg-2">
        Pin Code:
      </div>
      <div class="col-lg-4">
        <input type="textbox" id="txtPinCode" name="txtPinCode" maxlength="6" placeholder="Pin Code" value="<?php echo $profile_data['pin_code'] ?>"/>
      </div>
    </div>
    <div class="form-group">
      <div class="col-lg-2">
        Mobile Number:
      </div>
      <div class="col-lg-4">
        <input type="textbox" id="txtMobileNumber" name="txtMobileNumber" placeholder="Mobile number" value="<?php echo $profile_data['mobile_number'] ?>"/>
      </div>
    </div>
    <div class="form-group">
      <div class="col-lg-2">
      </div>
      <div class="col-lg-4">
      <input type="submit" id="btnSubmitProfile"  value="Update Profile " class="btn btn-primary" />
      </div>
    </div>
  </div>
</form>
  </div>
</div>
