<?php
if(!isset($_SESSION))
{
  session_start();
}
?>
<!--Search-->
<div id="search">
  <button type="button" class="close">×</button>
  <form>
    <input type="search" value="" placeholder="Search here...."  required/>
    <button type="submit" class="btn btn_common blue">Search</button>
  </form>
</div>

<!--Page Header-->
<section class="page_header padding-top">
  <div class="container">
    <div class="row">
      <div class="col-md-12 page-content">
        <?php
        if(isset($_SESSION["user"]))
        { ?>
          <h1><?php echo $_SESSION["name"] ?>'S Profile Password</h1>
        <?php } ?>
        <p>KnowledgeTime offers live online courses by best experts</p>
        <div class="page_nav">
          <span>You are here:<a href="<?php echo base_url();?>home/index">Home</a> <span><i class="fa fa-angle-double-right"></i>Profile</span>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="container">
  <div class="row">
    <form class="form-horizontal" method="post" Id="ChangeProfilePassword" action="<?php echo base_url();?>/users/student_dashboard/changeProfilePassword_CI">
      <div class="form-group">
        <div class="col-lg-2">
          New Password:
        </div>
        <div class="col-lg-4">
          <input type="password" id="txtNewPassword" name="txtNewPassword" class="form-control" placeholder="New Password.." required/>
        </div>
      </div>
      <div class="form-group">
        <div class="col-lg-2">
          Confirm Password:
        </div>
        <div class="col-lg-4">
          <input type="password" id="txtConfirmPassword" name="txtConfirmPassword" class="form-control" placeholder="Confirm Password" required/>
        </div>
      </div>
      <div class="form-group">
        <div class="col-lg-2"></div>
        <div class="col-lg-4">
          <input type="submit" id="btnChangeProfilePassword"  value="Change Password " class="btn btn-primary" />
        </div>
      </div>
    </div>
  </form>
</div>
</div>
