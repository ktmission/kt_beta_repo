
<!--Page Header-->
<section class="page_header padding-top">
  <div class="container">
    <div class="row">
      <div class="col-md-12 page-content">
        <h1>Courses</h1>
        <p>KnowledgeTime offers live online courses</p>
        <div class="page_nav">
      <span>You are here:</span> <a href="<?echo base_url();?>home/index">Home</a> <span><i class="fa fa-angle-double-right"></i>Courses</span>
      </div>
      </div>
    </div>
  </div>
</section>
<!--Page Header-->

<!-- Courses -->
<section id="course_all" class="padding-bottom">
  <div class="container">
    <div class="row">
      <div class="col-sm-6 col-md-4">
        <div class="course margin_top  wow fadeIn" data-wow-delay="700ms">
          <div class="image bottom25">
            <img src="<?php echo base_url();?>assets/images/courses/python.jpeg" alt="Course" class="border_radius">
          </div>
          <h3 class="bottom10"><a href="<?php echo base_url();?>courses/pythonProgramming">Python programming</a></h3>
          <p class="bottom20">KnowledgeTime offers live online courses</p>
          <a class="btn_common yellow border_radius" href="<?php echo base_url();?>courses/pythonProgramming">view details</a>
        </div>
      </div>
      <div class="col-sm-6 col-md-4">
        <div class="course margin_top wow fadeIn" data-wow-delay="500ms">
          <div class="image bottom25">
            <img src="<?php echo base_url();?>assets/images/courses/control_to_space.jpeg" alt="Course" class="border_radius">
          </div>
          <h3 class="bottom10"><a href="<?php base_url();?>courses/advancedControlEngineering">Advanced Control Engineering</a></h3>
          <p class="bottom20">KnowledgeTime offers live online courses</p>
          <a class="btn_common yellow border_radius" href="<?php base_url();?>courses/advancedControlEngineering">view details</a>
        </div>
      </div>
      <div class="col-sm-6 col-md-4">
        <div class="course margin_top wow fadeIn" data-wow-delay="600ms">
          <div class="image bottom25">
            <img src="<?php echo base_url();?>assets/images/courses/electronics_product_design.jpg" alt="Course" class="border_radius">
          </div>
          <h3 class="bottom10"><a href="<?php base_url();?>courses/electronicsProductDesign">Electronics product design</a></h3>
          <p class="bottom20">KnowledgeTime offers live online courses</p>
          <a class="btn_common yellow border_radius" href="<?php base_url();?>courses/electronicsProductDesign">view details</a>
        </div>
      </div>
    </div>
      <div class="row">
        <div class="col-sm-6 col-md-4">
          <div class="course margin_top wow fadeIn" data-wow-delay="400ms">
            <div class="image bottom25">
              <img src="<?php echo base_url();?>assets/images/courses/robotics.jpg" alt="Course" class="border_radius">
            </div>
            <h3 class="bottom10"><a href="<?php base_url();?>courses/roboticsDesign">Robotics</a></h3>
            <p class="bottom20">KnowledgeTime offers live online courses</p>
            <a class="btn_common yellow border_radius" href="<?php base_url();?>courses/roboticsDesign">view details</a>
          </div>
        </div>
        <div class="col-sm-6 col-md-4">
        <div class="course margin_top wow fadeIn" data-wow-delay="800ms">
          <div class="image bottom25">
            <img src="<?php echo base_url();?>assets/images/courses/data_analysis.jpeg" alt="Course" class="border_radius">
          </div>
          <h3 class="bottom10"><a href="<?php echo base_url();?>courses/dataAnalysis">Data Analysis</a></h3>
          <p class="bottom20">KnowledgeTime offers live online courses</p>
          <a class="btn_common yellow border_radius" href="data_analysis.php">view details</a>
        </div>
      </div>
      <div class="col-sm-6 col-md-4">
        <div class="course margin_top wow fadeIn" data-wow-delay="900ms">
          <div class="image bottom25">
            <img src="<?php echo base_url();?>assets/images/courses/internet_of_things.jpg" alt="Course" class="border_radius">
          </div>
          <h3 class="bottom10"> <a href="<?php echo base_url();?>courses/internetOfThings">Internet of Things</a></h3>
          <p class="bottom20">KnowledgeTime offers live online courses</p>
          <a class="btn_common yellow border_radius" href="<?php echo base_url();?>courses/internetOfThings">view details</a>
        </div>
      </div>
    </div>
    <div class="row">
        <div class="col-sm-6 col-md-4">
          <div class="course margin_top wow fadeIn" data-wow-delay="1000ms">
            <div class="image bottom25">
              <img src="<?php echo base_url();?>assets/images/courses/machine_learning.jpg" alt="Course" class="border_radius">
            </div>
            <h3 class="bottom10"><a href="<?php echo base_url();?>courses/machineLearning">Machine Learning</a></h3>
            <p class="bottom20">KnowledgeTime offers live online courses</p>
            <a class="btn_common yellow border_radius" href="<?php echo base_url();?>courses/machineLearning">view details</a>
          </div>
        </div>
        <div class="col-sm-6 col-md-4">
          <div class="course margin_top wow fadeIn" data-wow-delay="1100ms">
            <div class="image bottom25">
              <img src="<?php echo base_url();?>assets/images/courses/artificial_intelligence.jpeg" alt="Course" class="border_radius">
            </div>
            <h3 class="bottom10"><a href="<?php echo base_url();?>courses/artificialIntelligence">Artificial Intelligence</a></h3>
            <p class="bottom20">KnowledgeTime offers live online courses</p>
            <a class="btn_common yellow border_radius" href="<?php echo base_url();?>courses/artificialIntelligence">view details</a>
          </div>
        </div>
        <div class="col-sm-6 col-md-4">
          <div class="course margin_top wow fadeIn" data-wow-delay="1100ms">
            <div class="image bottom25">
              <img src="<?php echo base_url();?>assets/images/courses/artificial_intelligence.jpeg" alt="Course" class="border_radius">
            </div>
            <h3 class="bottom10"><a href="microfluidics.php">Microfluidics</a></h3>
            <p class="bottom20">KnowledgeTime offers live online courses</p>
            <a class="btn_common yellow border_radius" href="microfluidics.php">view details</a>
          </div>
        </div>
      </div>
    </div>
</section>
<!-- Courses ends -->
