<!--Search-->
<div id="search">
  <button type="button" class="close">×</button>
  <form>
    <input type="search" value="" placeholder="Search here...."  required/>
    <button type="submit" class="btn btn_common blue">Search</button>
  </form>
</div>


<!--Page Header-->
<section class="page_header padding-top">
  <div class="container">
    <div class="row">
      <div class="col-md-12 page-content">
        <h1>About US</h1>
        <p>KnowledgeTime offers live online courses by best experts</p>
        <div class="page_nav">
      <span>You are here:</span> <a href="<?php echo base_url(); ?>home/index">Home</a> <span><i class="fa fa-angle-double-right"></i>About Us</span>
      </div>
      </div>
    </div>
  </div>
</section>
<!--Page Header-->


<!--ABout US-->
<section id="about" class="padding">
  <div class="container aboutus">
    <div class="row">
      <div class="col-md-7 wow fadeInLeft" data-wow-delay="300ms">
       <h2 class="heading heading_space">Welcome to KnowledgeTime <span class="divider-left"></span></h2>
       <h4 class="bottom25">KnowledgeTime is a web-based platform that is designed to offer innovative and easily accessible lectures delivered by accomplished professors and industry experts.   </h4>
       <p class="bottom25">Highly passionate lecturers, professors and industrialists are our assets. This online platform allows students, the unique opportunity to learn the subjects of their interest at their own convenient time and pace. Our goal for the young minds is, to reach their dreams easier and faster. </p>
       <p class="bottom25">We offer all the students an opportunity to enhance their creative thinking, problem solving skills, find their passion and gain the required skills for their dream career</p>
      </div>
      <div class="col-md-5 wow fadeInRight" data-wow-delay="300ms">
        <div class="image">
         <img src="<?php echo base_url();?>assets/images/welcome.jpg" alt="Edua">
        </div>
      </div>
    </div>
  </div>
</section>
<!--ABout US-->

<!-- Company History -->
<!--  <section id="history" class="padding bg_grey">
  <div class="container">
    <div class="row">
      <div class="col-md-12 wow fadeInDown">
       <h2 class="heading heading_space">Company History <span class="divider-left"></span></h2>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6 col-sm-6 history_wrap bottom25 wow fadeIn" data-wow-delay="300ms">
        <div class="row">
          <div class="col-lg-5 col-md-5 col-sm-12">
            <div class="image"><img src="images/history1.jpg" alt="our history"></div>
          </div>
          <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
            <h3><span>1991</span> . Structure was Founded</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed eiusmod tempor incididunt ut labore et dolore magna aliqua.
            occaecat aute irure dolor in reprehenderit.</p>
          </div>
        </div>
      </div>
      <div class="col-md-6 col-sm-6 history_wrap bottom25 wow fadeIn" data-wow-delay="400ms">
        <div class="row">
          <div class="col-lg-5 col-md-5 col-sm-12">
            <div class="image"><img src="images/history2.jpg" alt="our history"></div>
          </div>
          <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
            <h3><span>1991</span> . Structure was Founded</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed eiusmod tempor incididunt ut labore et dolore magna aliqua.
            occaecat aute irure dolor in reprehenderit.</p>
          </div>
        </div>
      </div>
      <div class="col-md-6 col-sm-6 history_wrap bottom25 wow fadeIn" data-wow-delay="500ms">
        <div class="row">
          <div class="col-lg-5 col-md-5 col-sm-12">
            <div class="image"><img src="images/history3.jpg" alt="our history"></div>
          </div>
          <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
            <h3><span>1991</span> . Structure was Founded</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed eiusmod tempor incididunt ut labore et dolore magna aliqua.
            occaecat aute irure dolor in reprehenderit.</p>
          </div>
        </div>
      </div>
      <div class="col-md-6 col-sm-6 history_wrap bottom25 wow fadeIn" data-wow-delay="600ms">
        <div class="row">
          <div class="col-lg-5 col-md-5 col-sm-12">
            <div class="image"><img src="images/history4.jpg" alt="our history"></div>
          </div>
          <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
            <h3><span>1991</span> . Structure was Founded</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed eiusmod tempor incididunt ut labore et dolore magna aliqua.
            occaecat aute irure dolor in reprehenderit.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>     -->
<!-- Company History -->

<!--Fun Facts-->
<section id="counter" class="parallax padding">
  <div class="container">
    <h2 class="hidden">hidden</h2>
    <div class="row number-counters">
      <div class="col-md-3 col-sm-6 col-xs-6 counters-item text-center wow fadeInUp" data-wow-delay="300ms">
        <i class="icon-checkmark3"></i>
        <strong data-to="1235">0</strong>
        <p>Project Completed</p>
      </div>
      <div class="col-md-3 col-sm-6 col-xs-6 counters-item text-center wow fadeInUp" data-wow-delay="400ms">
        <i class="icon-trophy"></i>
        <strong data-to="78">0</strong>
        <p>Awards Won</p>
      </div>
      <div class="col-md-3 col-sm-6 col-xs-6 counters-item text-center wow fadeInUp" data-wow-delay="500ms">
        <i class=" icon-icons20"></i>
        <strong data-to="186">0</strong>
        <p>Hours of Work / Month</p>
      </div>
      <div class="col-md-3 col-sm-6 col-xs-6 counters-item text-center wow fadeInUp" data-wow-delay="600ms">
        <i class="icon-happy"></i>
        <strong data-to="89">0</strong>
        <p>Satisfied Clients</p>
      </div>
    </div>
  </div>
</section>
<!--Fun Facts-->




<!-- University Tour -->
<!--  <section id="tours" class="bg_grey padding">
  <div class="container tour_media">
    <div class="row">
      <div class="tour_body wow fadeInLeft" data-wow-delay="300ms">
        <h2 class="heading heading_space">University Tour<span class="divider-left"></span></h2>
        <h4 class="bottom25">It’s all going  about making ideas happen. Nor again is anyone who loves or pursues or desires </h4>
        <p class="bottom25">Proin sagittis feugiat elit finibus pretium. Donec et tortor non purus vulputate tincidunt. Cras congue posuer eros eget egestas.
          Aenean varius ex ut ex laoreet fermentum. odio Proin mattis congue tristique.
        </p>
        <a class="btn_common blue border_radius" href="#">Read more</a>
      </div>
      <div class="tour_feature wow fadeInRight" data-wow-delay="300ms">
        <img src="images/tour.jpg" alt="our Tour">
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div id="projects" class="cbp">
        <div class="cbp-item">
          <img src="images/tour1.jpg" alt="">
          <div class="overlay">
          <div class="centered text-center">
            <a href="images/tour1.jpg" class="cbp-lightbox opens"> <i class="icon-focus"></i></a>
          </div>
        </div>
        </div>
        <div class="cbp-item">
          <img src="images/tour2.jpg" alt="">
          <div class="overlay">
          <div class="centered text-center">
            <a href="images/tour2.jpg" class="cbp-lightbox opens"> <i class="icon-focus"></i></a>
          </div>
        </div>
        </div>
        <div class="cbp-item">
          <img src="images/tour3.jpg" alt="">
          <div class="overlay">
          <div class="centered text-center">
            <a href="images/tour3.jpg" class="cbp-lightbox opens"> <i class="icon-focus"></i></a>
          </div>
        </div>
        </div>
        <div class="cbp-item">
          <img src="images/tour4.jpg" alt="">
          <div class="overlay">
          <div class="centered text-center">
            <a href="images/tour4.jpg" class="cbp-lightbox opens"> <i class="icon-focus"></i></a>
          </div>
        </div>
        </div>
        <div class="cbp-item">
          <img src="images/tour5.jpg" alt="">
          <div class="overlay">
          <div class="centered text-center">
            <a href="images/tour5.jpg" class="cbp-lightbox opens"> <i class="icon-focus"></i></a>
          </div>
        </div>
        </div>
      </div>
    </div>
  </div>
</section>   -->
<!-- University Tour -->



<!-- Teachers -->
<!-- <section id="teachers" class="padding">
  <div class="container">
    <div class="row">
      <div class="col-md-12 wow fadeInDown">
        <h2 class="heading heading_space">Our Teachers<span class="divider-left"></span></h2>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="slider_wrapper">
          <div id="director_slider" class="owl-carousel">
            <div class="item">
              <div class="image bottom25">
                <img src="images/teacher1.jpg" alt="Teachers" class=" border_radius">
                <span class="post">App Developer</span>
              </div>
              <h3 class="bottom10">Mahmoud Baghagho</h3>
              <p class="bottom20">We offer the most complete house renovating services in the country, from kitchen design to bathroom remodeling.</p>
              <ul class="social_icon black bottom5">
                <li><a href="#." class="facebook"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#." class="twitter"><i class="icon-twitter4"></i></a></li>
                <li><a href="#." class="dribble"><i class="icon-dribbble5"></i></a></li>
              </ul>
            </div>
            <div class="item">
              <div class="image bottom25">
                <img src="images/teacher2.jpg" alt="Teachers" class=" border_radius">
                <span class="post">App Developer</span>
              </div>
              <h3 class="bottom10">Mahmoud Baghagho</h3>
              <p class="bottom20">We offer the most complete house renovating services in the country, from kitchen design to bathroom remodeling.</p>
              <ul class="social_icon black bottom5">
                <li><a href="#." class="facebook"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#." class="twitter"><i class="icon-twitter4"></i></a></li>
                <li><a href="#." class="dribble"><i class="icon-dribbble5"></i></a></li>
              </ul>
            </div>
            <div class="item">
              <div class="image bottom25">
                <img src="images/teacher3.jpg" alt="Teachers" class=" border_radius">
                <span class="post">App Developer</span>
              </div>
              <h3 class="bottom10">Mahmoud Baghagho</h3>
              <p class="bottom20">We offer the most complete house renovating services in the country, from kitchen design to bathroom remodeling.</p>
              <ul class="social_icon black bottom5">
                <li><a href="#." class="facebook"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#." class="twitter"><i class="icon-twitter4"></i></a></li>
                <li><a href="#." class="dribble"><i class="icon-dribbble5"></i></a></li>
              </ul>
            </div>
            <div class="item">
              <div class="image bottom25">
                <img src="images/teacher1.jpg" alt="Teachers" class=" border_radius">
                <span class="post">App Developer</span>
              </div>
              <h3 class="bottom10">Mahmoud Baghagho</h3>
              <p class="bottom20">We offer the most complete house renovating services in the country, from kitchen design to bathroom remodeling.</p>
              <ul class="social_icon blac bottom5k">
                <li><a href="#." class="facebook"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#." class="twitter"><i class="icon-twitter4"></i></a></li>
                <li><a href="#." class="dribble"><i class="icon-dribbble5"></i></a></li>
              </ul>
            </div>
            <div class="item">
              <div class="image bottom25">
                <img src="images/teacher2.jpg" alt="Teachers" class=" border_radius">
                <span class="post">App Developer</span>
              </div>
              <h3 class="bottom10">Mahmoud Baghagho</h3>
              <p class="bottom20">We offer the most complete house renovating services in the country, from kitchen design to bathroom remodeling.</p>
              <ul class="social_icon black bottom5">
                <li><a href="#." class="facebook"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#." class="twitter"><i class="icon-twitter4"></i></a></li>
                <li><a href="#." class="dribble"><i class="icon-dribbble5"></i></a></li>
              </ul>
            </div>
            <div class="item">
              <div class="image bottom25">
                <img src="images/teacher3.jpg" alt="Teachers" class=" border_radius">
                <span class="post">App Developer</span>
              </div>
              <h3 class="bottom10">Mahmoud Baghagho</h3>
              <p class="bottom20">We offer the most complete house renovating services in the country, from kitchen design to bathroom remodeling.</p>
              <ul class="social_icon black bottom5">
                <li><a href="#." class="facebook"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#." class="twitter"><i class="icon-twitter4"></i></a></li>
                <li><a href="#." class="dribble"><i class="icon-dribbble5"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>  -->
<!-- Teachers -->
-->
