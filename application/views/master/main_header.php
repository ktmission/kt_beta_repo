<?php session_start(); ?>
<html>
<header>
  <nav class="navbar navbar-default navbar-fixed white no-background bootsnav">
    <div class="container">
       <div class="search_btn btn_common"><i class="icon-icons185"></i></div>
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
          <i class="fa fa-bars"></i>
        </button>
        <a class="navbar-brand" href="<?php echo base_url() ;?>home/index"><img src="<?php echo base_url();?>assets/images/logo-white.png" alt="logo" class="logo logo-display" alt style="  width: 280px; height: 47px; ">
          <img src="<?php echo base_url();?>assets/images/logo.png" class="logo logo-scrolled" alt style="  width: 280px; height: 47px; ">
        </a>
      </div>
      <div class="collapse navbar-collapse" id="navbar-menu">
        <ul class="nav navbar-nav navbar-right" data-in="fadeInDown" data-out="fadeOut">
          <li>
            <a href="<?php echo base_url() ;?>home/index" class="dropdown-toggle" data-toggle="dropdown" >Home</a>
           </li>
          <li>
            <a href="<?php echo base_url();?>courses" class="dropdown-toggle" data-toggle="dropdown" id="usercourses">courses</a>
            </li>
        <!--  <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" >events</a>
            <ul class="dropdown-menu">
              <li><a href="event.php">events</a></li>
              <li><a href="event_detail.php">Events Detail</a></li>
            </ul>
          </li>
-->
        <!--  <li class="dropdown megamenu-fw">-->
            <li>
            <a href="<?php echo base_url();?>public/public_ci/knowledgeTimeAbout" class="dropdown-toggle" data-toggle="dropdown">About</a>
          </li>
          <!--  <ul class="dropdown-menu megamenu-content" role="menu">
              <li>
                <div class="row">
                  <div class="col-menu col-md-3">
                    <h6 class="title">Pages</h6>
                    <div class="content">
                      <ul class="menu-col">
                        <li><a href="about.php">About</a></li>
                        <li><a href="testinomial.php">Testinomial</a></li>
                        <li><a href="our_team.php">Our team</a></li>
                  <li><a href="pricing.php">Pricings</a></li>
                      </ul>
                    </div>
                  </div>
                  <div class="col-menu col-md-3">
                    <h6 class="title">Blog</h6>
                    <div class="content">
                      <ul class="menu-col">
                        <li><a href="blog.php">Blog</a></li>
                        <li><a href="blog/blog2.php">Blog 02</a></li>
                        <li><a href="blog/blog3.php">Blog 03</a></li>
                        <li><a href="blog_detail.php">Blog Detail</a></li>
                      </ul>
                    </div>
                  </div>
                  <div class="col-menu col-md-3">
                    <h6 class="title">Shop</h6>
                    <div class="content">
                      <ul class="menu-col">
                        <li><a href="shop.php">Shop</a></li>
                        <li><a href="shop_detail.php">Shop Detail</a></li>
                        <li><a href="shop_cart.php">Cart</a></li>
                      </ul>
                    </div>
                  </div>
                  <div class="col-menu col-md-3">
                    <h6 class="title">Others</h6>
                    <div class="content">
                      <ul class="menu-col">
                        <li><a href="gallery.php">Gallery</a></li>
                        <li><a href="faq.php">Faq</a></li>
                        <li><a href="404.php">404</a></li>
                      </ul>
                    </div>
                  </div>
                </div>
              </li>
            </ul>
          </li>-->
          <li><a href="<?php echo base_url();?>public/blog_ci/knowledgeTimeBlog">Blog</a></li>
          <li><a href="<?php echo base_url();?>public/public_ci/knowledgeTimeContact">Contact</a></li>
<?php
 if(isset($_SESSION["user"]))
 { ?>
          <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" > <?php echo $_SESSION["name"] ?></a>
              <ul class="dropdown-menu">
                <li><a href="<?php echo base_url(); ?>users/student_dashboard/studentAccount" name="Account">Account</a></li>
                <li><a href="<?php echo base_url(); ?>users/student_dashboard/GetChangePasswordPage" name="Change Password">Change Password</a></li>
                <li><a href="<?php echo base_url() ;?>login_handler/userLogOut" name="logout">Logout</a></li>
              </ul>
            </li>
          <?php }else{ ?>
            <!-- it will call login_handler/userLogin -->
          <li><a href="<?php echo base_url() ;?>login_handler/userLogin">Login</a></li>
<?php } ?>
        </ul>
      </div>
    </div>
  </nav>
</header>
</html>
