<!--Search-->
<div id="search">
  <button type="button" class="close">×</button>
  <form>
    <input type="search" value="" placeholder="Search here...."  required/>
    <button type="submit" class="btn btn_common blue">Search</button>
  </form>
</div>


<!--Page Header-->
<section class="page_header padding-top">
  <div class="container">
    <div class="row">
      <div class="col-md-12 page-content">
        <h1>Blog</h1>
        <p>KnowledgeTime offers live online courses by best experts</p>
        <div class="page_nav">
      <span>You are here:</span> <a href="<?php echo base_url();?>home/index">Home</a> <span><i class="fa fa-angle-double-right"></i>Blog</span>
      </div>
      </div>
    </div>
  </div>
</section>

<!--BLOG SECTION-->
<section id="blog" class="padding-bottom-half padding-top">
 <h3 class="hidden">hidden</h3>
 <div class="container">
     <div class="row">
      <div class="col-md-9 col-sm-8 wow fadeIn" data-wow-delay="400ms">
        <article class="blog_item padding-bottom-half heading_space">
          <div class="image bottom25">
            <img src="<?php echo base_url();?>assets/images/blog/blogfull1.jpg" alt="blog">
          </div>
          <h3>Education- An unceasing trail of changes</h3>
          <ul class="comment margin10">
            <li><a href="#.">Feb 20 2017</a></li>
            <li><a href="#."><i class="icon-comment"></i> 5</a></li>
          </ul>
          <p class="margin10">Google defines education as ‘The process of receiving or giving systematic instruction, especially at a school or university’. The second definition says - ‘An enlightening experience’. Put together rightfully, education is indeed an enlightening experience we go through by receiving systematic instructions not just at schools or universities, but in the later parts as well. Education forms the base of the society, standing as the core pillar to every other industry, be it communications, textiles, chemicals or trade. </p>
          <p class="margin10">Time and again, the process through which education is being channelized has gone through a tremendous yet gradual change, blending itself with the needs of various generations. Listed below are some of the paradigm shifts which are making a pathbreaking impact on the education system today.</p>
          <p class="margin10">From convergence to gradual divergence: Convergent systems are where a group of students/learners are directed and guided by the tutor who has the necessary skills to equip the students with the subject of discussion. While divergent systems encourage self learning and abilities enhancement of the students. Gone are the times when students were expected to blindly follow the guidelines the tutors told and take the task to completion. The current system is getting into a balanced amalgam of convergent and divergent, where the tutor aims to teach the art of fishing rather than feeding the learner with one fish per day.
</p>
          <p class="margin10">Skill based learning:Bringing in real time scenarios and projects are always the best bet. Having an hands-on experience on any skill along with the lengthy definitions and formula, gives the learner an all new strong perspective of the subject. That way, he will be able to relate, replicate and employ his learnings quickly and aptly.
</p>
          <p class="margin10">The advent of online tutorials:With technology seeping into every aspect of our lives, it’s penetration into education has given an all new face to learning. Online tutorials are making it convenient as of time and geographies are concerned, since learners can choose to learn, anytime, anywhere. Efforts have been made to make sure education reaches the remotest parts of the world through smart phones and other devices which can connect to the internet and harness the knowledge.

As said by Albert Einstein, ‘Education is not learning of the facts, but the training of the minds to think’. And, these newer approaches would greatly create an environment to think rather than mere learning!
</p>

        </article>
        <div class="share clearfix heading_space">
          <p class="pull-left"><strong>Share This Article:</strong></p>
          <ul class="pull-right">
            <li><a href="#."><i class="fa fa-facebook"></i></a></li>
            <li><a href="#."><i class="icon-twitter4"></i></a></li>
            <li><a href="#."><i class="icon-dribbble5"></i></a></li>
            <li><a href="#."><i class="icon-instagram"></i></a></li>
            <li><a href="#."><i class="icon-vimeo4"></i></a></li>
        </ul>
        </div>
        <div class="row">
        <div class="col-md-6">
        <article class="blog_newest text-left heading_space border_radius">
          <h2 class="hidden">Share This Article:</h2>
          <span class="post_img"><img src="<?php echo base_url();?>assets/images/post3.png" alt="newest"></span>
          <div class="text">
          <i class="link">Previous Post</i>
          <a href="#." class="post_title">Before Making your Dream Room that through a foe market.</a>
          </div>
          </article>
        </div>
        <div class="col-md-6">
        <article class="blog_newest text-right heading_space border_radius">
          <h2 class="hidden">Share This Article:</h2>
          <div class="text">
          <i class="link">Next Post</i>
          <a href="#." class="post_title">Before Making your Dream Room that through a foe market.</a>
          </div>
          <span class="post_img"><img src="<?php echo base_url();?>assets/images/post1.png" alt="newest"></span>
          </article>
        </div>
        </div>
        <article>
        <h3 class="heading bottom25">3 Comments<span class="divider-left"></span></h3>
        <div class="profile_border">
          <div class="profile top20 bottom20">
            <div class="p_pic"><img src="<?php echo base_url();?>assets/images/profile2.png" alt="instructure"></div>
            <div class="profile_text">
              <h5><strong>JOHN PARKER</strong>  -  <span> Great for Starters</span></h5>
              <ul class="comment">
                 <li><a href="#.">Jan 28, 2016 - 10:07 pm</a></li>
              </ul>
              <p>Vivamus bibendum nibh in dolor pharetra, a euismod nulla dignissim. Aenean viverra tincidunt nibh, in imperdiet nunc. Suspendisse eu ante pretium.</p>
              <a class="readmore" href="#.">LEAVE A REPLY</a>
            </div>
          </div>
        </div>
        <div class="profile_border">
          <div class="profile top20 bottom20">
            <div class="p_pic"><img src="<?php echo base_url();?>assets/images/profile3.png" alt="instructure"></div>
            <div class="profile_text">
              <h5><strong>JOHN PARKER</strong>  -  <span>Excellent Work</span></h5>
              <ul class="comment">
                 <li><a href="#.">Jan 28, 2016 - 10:07 pm</a></li>
              </ul>
              <p>Vivamus bibendum nibh in dolor pharetra, a euismod nulla dignissim. Aenean viverra tincidunt nibh, in imperdiet nunc. Suspendisse eu ante pretium.</p>
              <a class="readmore" href="#.">LEAVE A REPLY</a>
            </div>
          </div>
        </div>
        <div class="profile_border heading_space">
          <div class="profile top20 bottom20">
            <div class="p_pic"><img src="<?php echo base_url();?>assets/images/profile4.png" alt="instructure"></div>
            <div class="profile_text">
              <h5><strong>JOHN PARKER</strong>  -  <span>Awesome Quality</span></h5>
              <ul class="comment">
                 <li><a href="#.">Jan 28, 2016 - 10:07 pm</a></li>
              </ul>
              <p>Vivamus bibendum nibh in dolor pharetra, a euismod nulla dignissim. Aenean viverra tincidunt nibh, in imperdiet nunc. Suspendisse eu ante pretium.</p>
              <a class="readmore" href="#.">LEAVE A REPLY</a>
            </div>
          </div>
        </div>
        <h2 class="heading bottom25">Leave A Reply<span class="divider-left"></span></h2>
        <form class="findus heading_space">
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Name" required>
        </div>
        <div class="form-group">
          <input type="email" class="form-control" placeholder="Email" required>
        </div>
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Website" required>
        </div>
        <textarea placeholder="Comment"></textarea>
        <button type="submit" class="btn_common yellow border_radius">post your comment</button>
      </form>
        </article>
      </div>
      <div class="col-md-3 col-sm-4 wow fadeIn" data-wow-delay="400ms">
        <aside class="sidebar bg_grey border-radius">
          <div class="widget heading_space">
            <form class="widget_search border-radius">
              <div class="input-group">
                <input type="search" class="form-control" placeholder="Search Here" required>
                <i class="input-group-addon icon-icons185"></i>
              </div>
            </form>
          </div>
          <div class="widget heading_space">
            <h3 class="bottom20">Featured Courses</h3>
            <div class="media">
              <a class="media-left" href="#."><img src="<?php echo base_url();?>assets/images/post1.png" alt="post"></a>
              <div class="media-body">
                <h5 class="bottom5">Artificial Intelligence</h5>
                <a href="#." class="btn-primary border_radius bottom5">free</a>
                <form class="star_rating">
                  <div class="stars">
                    <input type="radio" name="star" class="star-1" id="star-1" />
                    <label class="star-1" for="star-1">1</label>
                    <input type="radio" name="star" class="star-2" id="star-2" />
                    <label class="star-2" for="star-2">2</label>
                    <input type="radio" name="star" class="star-3" id="star-3" />
                    <label class="star-3" for="star-3">3</label>
                    <input type="radio" name="star" class="star-4" id="star-4" checked  />
                    <label class="star-4" for="star-4">4</label>
                    <input type="radio" name="star" class="star-5"  id="star-5" />
                    <label class="star-5" for="star-5">5</label>
                    <span></span>
                  </div>
                </form>
                <span class="name">Michael Windzor</span>
              </div>
            </div>
            <div class="media">
              <a class="media-left" href="#."><img src="<?php echo base_url();?>assets/images/post1.png" alt="post"></a>
              <div class="media-body">
                <h5 class="bottom5">Artificial Intelligence</h5>
                <a href="#." class="btn-primary border_radius bottom5">free</a>
                <form class="star_rating">
                  <div class="stars">
                    <input type="radio" name="star" class="star-1" id="star-6" />
                    <label class="star-1" for="star-6">1</label>
                    <input type="radio" name="star" class="star-2" id="star-7" />
                    <label class="star-2" for="star-7">2</label>
                    <input type="radio" name="star" class="star-3" id="star-8" />
                    <label class="star-3" for="star-8">3</label>
                    <input type="radio" name="star" class="star-4" id="star-9"  />
                    <label class="star-4" for="star-9">4</label>
                    <input type="radio" name="star" class="star-5"  id="star-10" checked  />
                    <label class="star-5" for="star-10">5</label>
                    <span></span>
                  </div>
                </form>
                <span class="name">Michael Windzor</span>
              </div>
            </div>
            <div class="media">
              <a class="media-left" href="#."><img src="<?php echo base_url();?>assets/images/post1.png" alt="post"></a>
              <div class="media-body">
                <h5 class="bottom5">Artificial Intelligence</h5>
                <a href="#." class="btn-primary border_radius bottom5">free</a>
                <form class="star_rating">
                  <div class="stars">
                    <input type="radio" name="star" class="star-1" id="star-01" />
                    <label class="star-1" for="star-01">1</label>
                    <input type="radio" name="star" class="star-2" id="star-02" />
                    <label class="star-2" for="star-02">2</label>
                    <input type="radio" name="star" class="star-3" id="star-03" />
                    <label class="star-3" for="star-03">3</label>
                    <input type="radio" name="star" class="star-4" id="star-04"  />
                    <label class="star-4" for="star-04">4</label>
                    <input type="radio" name="star" class="star-5"  id="star-05" checked  />
                    <label class="star-5" for="star-05">5</label>
                    <span></span>
                  </div>
                </form>
                <span class="name">Michael Windzor</span>
              </div>
            </div>
          </div>
          <div class="widget heading_space">
            <h3 class="bottom20">Top Tags</h3>
            <ul class="tags">
              <li><a href="#.">Books</a></li>
              <li><a href="#.">Midterm test </a></li>
              <li><a href="#.">Presentation</a></li>
              <li><a href="#.">Courses</a></li>
              <li><a href="#.">Certifications</a></li>
              <li><a href="#.">Our team</a></li>
              <li><a href="#.">Student Life</a></li>
              <li><a href="#.">Study</a></li>
              <li><a href="#.">Midterm test </a></li>
              <li><a href="#.">Presentation</a></li>
              <li><a href="#.">Courses</a></li>
            </ul>
          </div>
        </aside>
      </div>
    </div>
  </div>
</section>
