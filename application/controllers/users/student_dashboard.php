<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Student_Dashboard extends CI_Controller
{
  public function index()
  {

  }
  public function studentAccount()
  {
    //Country Data
    $this->load->model('user_dashboard/user_model','user_model');
    $countryData['countries']=$this->user_model->getCountryMaster();
    $profile_data['profile_data']=$this->user_model->getStudentProfileDetails();
    $stateList['state_list']=  $this->user_model->getStateMaster($profile_data['profile_data']['country_id']);
    $cityList['city_list']=  $this->user_model->getCityMaster($profile_data['profile_data']['state_id']);
    $data = array_merge($countryData, $profile_data,$stateList,$cityList);
    $this->template->content->view('users/student/student_profile',$data);//$profile_data,$countryData);
    $this->template->publish();
  }
  //Update Student/User Profile
  public function updateStudentProfile_CI()
  {
    if(isset($_SESSION["RegistrationID"]))
    {
      $userRegId=$this->session->userdata('RegistrationID');
      $txtFirstName=$this->input->post('txtFirstName');
      $txtMiddleName=$this->input->post('txtMiddleName');
      $txtLastName=$this->input->post('txtLastName');
      $txtAddressLine1=$this->input->post('txtAddressLine1');
      $txtAddressLine2=$this->input->post('txtAddressLine2');
      $intCountryID=$this->input->post('countryid');
      $intStateID=$this->input->post('stateid');
      $txtPinCode=$this->input->post('txtPinCode');
      $txtMobileNumber=$this->input->post('txtMobileNumber');
      $studentProfileData=array('txtFirstName'=>$txtFirstName,'txtMiddleName'=>$txtMiddleName,'txtLastName'=>$txtLastName,
      'txtAddressLine1'=>$txtAddressLine1,'txtAddressLine2'=>$txtAddressLine2,'intCountryID'=>$intCountryID,
      'intStateID'=>$intStateID,'txtPinCode'=>$txtPinCode,'txtMobileNumber'=>$txtMobileNumber);
      $this->load->model('user_dashboard/user_model','registration');
      $isProfileUpdated= $this->registration->updateStudentProfile($studentProfileData);
      if($isProfileUpdated==true)
      {
        echo "<script>alert('Your Profile Updated Successfully');window.location.href='" . base_url() . "users/student_dashboard/studentAccount';</script>";
      }
      else {
        echo "<script>alert('Failed To Update Profile.');window.location.href='" . base_url() . "users/student_dashboard/studentAccount' ;</script>";
      }
    }
    else {
      echo "<script>alert('Your session has expired Login to update profile.');window.location.href='" . base_url() . "login_handler/userLogin';</script>";
    }
  }
  //Bind Change User Password
  public function GetChangePasswordPage()
  {
    $this->template->content->view('users/student/change_password');//$profile_data,$countryData);
    $this->template->publish();
  }
  //Change User Password
  public function changeProfilePassword_CI()
  {
    if(isset($_SESSION["RegistrationID"]))
    {
      $userRegId=$this->session->userdata('RegistrationID');
      $this->form_validation->set_rules('txtNewPassword', 'Password', 'required');
      $this->form_validation->set_rules('txtConfirmPassword', 'Confirm Password', 'required|matches[txtNewPassword]');
      if ($this->form_validation->run())
      {
        $txtPassword=$this->input->post('txtNewPassword');
        $profilePassword=array('txtPassword'=>$txtPassword,'userRegId'=>$userRegId);
        $this->load->model('user_dashboard/user_model','user');
        $isPassWordUpdated= $this->user->changeProfilePassword_CM($profilePassword);
        if($isPassWordUpdated==true)
        {
          echo "<script>alert('Your Profile Password Updated Successfully');window.location.href='" . base_url() . "users/student_dashboard/GetChangePasswordPage';</script>";
        }
        else {
          echo "<script>alert('Failed To Update Password Profile.');window.location.href='" . base_url() . "users/student_dashboard/GetChangePasswordPage' ;</script>";
        }
      }
      else {
        echo "<script>alert('New Password and Confirm Password does not match. Try again!');window.location.href='" . base_url() . "users/student_dashboard/GetChangePasswordPage';</script>";
      }
    }
    else {
      echo "<script>alert('Your session has expired Login to update Password.');window.location.href='" . base_url() . "login_handler/userLogin';</script>";
    }
  }
}
?>
