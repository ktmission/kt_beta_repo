<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Courses extends CI_Controller
{
public function index()
{

}
public function userCourses()
{
  $this->template->content->view('pages/courses');
  $this->template->publish();
}
public function pythonProgramming()
{
  $this->template->content->view('pages/python_programming');
  $this->template->publish();
}
public function advancedControlEngineering()
{
  $this->template->content->view('pages/advanced_control_engineering');
  $this->template->publish();
}
public function electronicsProductDesign()
{
  $this->template->content->view('pages/electronics_product_design');
  $this->template->publish();
}
//robotics
public function roboticsDesign()
{
  $this->template->content->view('pages/robotics');
  $this->template->publish();
}
//call data_analysis.php page i.e inside->view->pages->data_analysis.php
public function dataAnalysis()
{
  $this->template->content->view('pages/data_analysis');
  $this->template->publish();
}
//call data_analysis.php page i.e inside->view->pages->machine_learning.php
public function machineLearning()
{
  $this->template->content->view('pages/machine_learning');
  $this->template->publish();
}
//call data_analysis.php page i.e inside->view->pages->artificial_intelligence.php
public function artificialIntelligence()
{
  $this->template->content->view('pages/artificial_intelligence');
  $this->template->publish();
}
//call data_analysis.php page i.e inside->view->pages->artificial_intelligence.php
public function internetOfThings()
{
  $this->template->content->view('pages/internet_of_things');
  $this->template->publish();
}
}
?>
