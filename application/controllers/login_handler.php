<?php
class Login_Handler extends CI_Controller
{
  public function index()
  {

  }
  public function userLogin()
  {
    //Load model for country List
    $this->load->model('user_dashboard/user_model','country_list');
    $countryData['countries']=$this->country_list->getCountryMaster();
    //passing model countryData to view so that it can be accessible from login.php page
    $this->template->content->view('public/login',$countryData);
    $this->template->publish();
  }
  //Verify user's Login UserID and password
  public function verifyUserLoginCredentials()
  {
    $username=$this->input->post('loginEmail');
    $userpassword=$this->input->post('loginpwd');
    $this->load->model('user_dashboard/user_model','userDashoard');
    $IsLoginSuccess=$this->userDashoard->setUserLoginSession($username,$userpassword);
    if($IsLoginSuccess)
    {
      echo "Login Success" ;
      redirect('home/index');
    }
    else {
      echo "login failed";
      redirect('login_handler/userLogin');
      //  echo '<script type="text/javascript">alert("' . $_GLOBALS['message'] . '")</script>';
    }
  }
  // Call when user try to register himself on KT
  public function setUserRegistration()
  {  // Form_validations
    //$this->form_validation->set_rules('firstName','First Name Required.','require');
    // $this->form_validation->set_rules('email','Email ID','require|valid_email|trim');
    // $this->form_validation->set_rules('pwd','Password','require|md5|trim');
    // $this->form_validation->set_rules('repwd','Confirm Password','require|md5|trim|matches[pwd]');
    // if($this->form_validation->run()==FALSE)
    // {
    //     echo 'error';          // $this->session->set_flashdata('SignupMsg',validation_errors());
    //     //redirect('login_handler/userLogin');
    //     print_r(validation_errors());
    // }
    // else
    // {
    //
    // }
    $txtFirstName=$this->input->post('firstName');
    $txtEmailID=$this->input->post('email');
    $txtPassword=$this->input->post('pwd');
    $intRoleID=$this->input->post('registerAs');
    $intCountryID=$this->input->post('countryid');
    $txtMobileNo=$this->input->post('pNumber');
    $UserRegData=array('txtFirstName'=>$txtFirstName,'txtEmailID'=>$txtEmailID,'txtPassword'=>$txtPassword,
    'intRoleID'=>$intRoleID,'intCountryID'=>$intCountryID,'txtMobileNo'=>$txtMobileNo);
    $this->load->model('user_dashboard/user_model','registration');
    $isRegistered= $this->registration->NewUserRegistration($UserRegData);
    //print_r($isRegistered);
    if($isRegistered==true)
    {
      //Showing Error Message as well as redirecting to index page
      echo "<script>alert('Your Account Created Successfully');window.location.href='" . base_url() . "home/index';</script>";
    }
    else {
      echo "Registeation failed!";
      //echo "<script>alert('Registeation failed');window.location.href='" . base_url() . "login_handler/userLogin';</script>";
      redirect('login_handler/userLogin');
    }
  }
  //Destroy User Session on Logout
  public function userLogOut()
  {
    //unset multiple session variable
    $destroySessionData = array('user' =>'','userid'=>'','name'=>'');
    $this->session->unset_userdata($destroySessionData);
    // destroy the session
    $this->session->sess_destroy();
    echo '<script type="text/javascript">alert("You are Loggged Out Successfully.")</script>';
    redirect('home/index');
  }
}
?>
