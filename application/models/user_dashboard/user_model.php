<?php
class User_Model extends CI_Model
{
  public function setUserLoginSession($username,$userpassword)
  {
    //Encrpting user password
    $login_password=md5($userpassword);
    $this->db->where('login_userid',$username);
    //  $this->db->where('password',$login_password);
    $queryResult=  $this->db->get('registered_users');
    if($queryResult->num_rows()==1)
    {
      $upassword = $queryResult->row()->password;
      if($login_password == $upassword)
      {
        // Setting session data in Array for user
        $sessionData = array('user' =>$queryResult->row()->login_userid,'RegistrationID'=>$queryResult->row()->registration_id,'name'=>$queryResult->row()->first_name);
        //print_r($sessionData);
        $this->session->set_userdata($sessionData);
        return true;
      }
      else
      {
        $this->session->set_flashdata('message','Check Your UserName and Password');
        echo '<script type="text/javascript">alert("Check Your UserName and Password.")</script>';
        return false;
      }
    }
    else {
      $this->session->set_flashdata('message','Please Check Your user name and Password.');
      echo '<script type="text/javascript">alert("User does not exists.")</script>';
      return false;
    }
  }
  public function getCountryMaster()
  {
    $queryCountryList=$this->db->select(['id','name'])->get('countries');
    //$userquery= $queryCountryList->result();
    //echo '<script type="text/javascript">alert("'.$queryCountryList->result().'")</script>';
    // echo "<pre>";
    // print_r($userquery);
    // echo "</pre>";
    return $queryCountryList->result();

  }
  public function getCityMaster($StateID)
  {
    if(isset($StateID))
    {
      $this->db->where('state_id',$StateID);
    }
    else {
      $this->db->where('1','1');
    }
    $queryCityList=$this->db->select(['id','name'])->get('cities');
    return $queryCityList->result();

  }
  public function getStateMaster($countryID)
  {
    $this->db->where('country_id',$countryID);
    $queryStateList=$this->db->select(['id','name'])->get('states');
    return $queryStateList->result();
  }

  //Register New User
  public function NewUserRegistration($RegArrayData)
  {
    //array('intRoleID'=>$intRoleID,'intCountryID'=>$intCountryID,'txtMobileNo'=>$txtMobileNo);
    //extract Field from $RegArrayData Array
    $txtFirstName=$RegArrayData['txtFirstName'];
    $txtEmailID=$RegArrayData['txtEmailID']; //userId of registrant
    $txtUserPwd=$RegArrayData['txtPassword'];
    $txtUserPwd=md5($txtUserPwd);
    $intUserCountryID=$RegArrayData['intCountryID'];
    $roleID=$RegArrayData['intRoleID'];
    $txtMobileNumber=$RegArrayData['txtMobileNo'];
    /* First Check whether User Already exists*/
    $this->db->where('login_userid',$txtEmailID);
    $queryResult=  $this->db->get('registered_users');
    //User Already Exists
    if($queryResult->num_rows()==1)
    {
      $this->session->set_flashdata('SignupMsg','This Email has already been registered. Try with new Email address.');
      echo "<script>alert('Registeation failed');</script>";
      return false;
    }
    else {
      //Inserting Data into registered_users Table
      $queryStatus= $this->db->insert('registered_users',array('login_userid'=>$txtEmailID,'password'=>$txtUserPwd,'role_id'=>$roleID,
      'first_name'=>$txtFirstName,'country_id'=>$intUserCountryID,'mobile_number'=>$txtMobileNumber,'email_id'=>$txtEmailID,'active'=>1));
      if(!$queryStatus)
      {
        $error=$this->db->error();
        return false;
      }
      else {
        //echo 'User Registered Successfully.';
        $this->session->set_flashdata('SignupMsg','Your account has been created successfully!');
        $this->setUserLoginSession($RegArrayData['txtEmailID'],$RegArrayData['txtPassword']);
        return true;
      }
    }
  }
  //get Register User profile Details
  public function  getStudentProfileDetails()
  {
    $userRegId=$this->session->userdata('RegistrationID');
    //  $UserProfileDataQuery=$this->db->select('first_name,middle_name,last_name,address1,address2,pin_code,mobile_number')
    //                          ->from ('registered_users')
    //                        ->where('registration_id',$userid)
    //                      ->get();
    $Sql="Select login_userid,first_name,middle_name,last_name,address1,address2,pin_code,mobile_number,country_id,state_id,city_id from registered_users where registration_id=?";
    $fetchQuery=$this->db->query($Sql,array($userRegId));
    $queryResult=$fetchQuery->row_array();
    return $queryResult;
  }
  public function updateStudentProfile($arrProfileData)
  {
    $userRegId=$this->session->userdata('RegistrationID');
    $txtFirstName=$arrProfileData['txtFirstName'];
    $txtMiddleName=$arrProfileData['txtMiddleName'];
    $txtLastName=$arrProfileData['txtLastName'];
    $txtAddressLine1=$arrProfileData['txtAddressLine1'];
    $txtAddressLine2=$arrProfileData['txtAddressLine2'];
    $intCountryID=$arrProfileData['intCountryID'];
    $intStateID=$arrProfileData['intStateID'];
    $countryID=$arrProfileData['intCountryID'];
    $txtMobileNumber=$arrProfileData['txtMobileNumber'];
    $txtPinCode=$arrProfileData['txtPinCode'];

    $this->db->where('registration_id=',$userRegId);
    $queryStatus= $this->db->update('registered_users',array('first_name'=>$txtFirstName,'middle_name'=>$txtMiddleName,'last_name'=>$txtLastName,
    'address1'=>$txtAddressLine1,'address2'=>$txtAddressLine2,'country_id'=>$intCountryID,'state_id'=>$intStateID,'mobile_number'=>$txtMobileNumber,
    'pin_code'=>$txtPinCode));
    if(!$queryStatus)
    {
      $error=$this->db->error();
      return false;
    }
    else {
      //echo 'User profile updated Successfully.';
      return true;
    }
  }
  public function changeProfilePassword_CM($arrProfilePassword)
  {
    $userRegId=$arrProfilePassword['userRegId'];
    $txtPassword=$arrProfilePassword['txtPassword'];
    $txtPassword=md5($txtPassword);
    $queryStatus=$this->db->where('registration_id',$userRegId);
    $this->db->update('registered_users',array('password'=>$txtPassword));
    if($queryStatus)
    {
      return true;
    }
    else
    {
      return false;
    }
  }
  public function __construct()
  {
    $this->load->database();
  }
}
?>
